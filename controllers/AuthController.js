import bcrypt from 'bcrypt';
import {validationResult} from "express-validator";
import JWT from 'jsonwebtoken';

import secret from '../config.js';
import Admins from "../models/Admins.js";



const generateAccessToken = (id, username) => {
    const payload = {
        id,
        username
    }
    return JWT.sign(payload, secret, {expiresIn: "24h"})
}

class AuthController {
    async registartion(req, res) {
        try {

            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                res.status(400).json({message: "Ошибка регистрации", errors})
            }
            else  {
                const {username, first_name, phone, password} = req.body;
                const candidate =  await Admins.findOne({username});
                if (candidate) {
                    res.status(400).json({message: "Пользователь уже существует"})
                }
                let hashPass = bcrypt.hashSync(password, 6);
                const user = new Admins({username, password: hashPass, first_name, phone});
                await user.save();
                return res.json(user);
            }

        } catch(err) {
            res.status(500).json(err);
        }
    }
    async login(req, res) {
        try {
            const {username, password} = req.body;
            const candidate =  await Admins.findOne({username});
            if (!candidate) {
                res.status(400).json({message: "Пользователь с таким логином не найден"})
            }
            else  {
                const validPass =  bcrypt.compareSync(password, candidate.password)
                if (!validPass) {
                    res.status(400).json({message: "Введен неверный пароль"})
                }
                else {
                    const token = generateAccessToken(candidate._id, candidate.username);
                    return res.json({token})
                }
            }

        } catch(err) {
            res.status(500).json(err);
        }
    }
}

export default new AuthController();