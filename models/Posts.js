import mongoose from 'mongoose'

const Post = new mongoose.Schema({
    first_name: {type: String, required: true},
    city: {type: String, required: true},
    rate: {type: Number, required: true},
    content: {type: String, required: true},
    approve: {type: Boolean, required: true}
})

export default mongoose.model('Post', Post);