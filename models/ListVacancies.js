import mongoose from "mongoose";

const ListVacancies = new mongoose.Schema({
    type_job: {type: String, required: true},
    description: {type: String, required: true},
});

export default mongoose.model('ListVacancies', ListVacancies);