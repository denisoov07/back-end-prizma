import mongoose from "mongoose";

const Orders = new mongoose.Schema({
    first_name: {type: String, required: true},
    email: {type: String, required: true},
    phone: {type: String, required: true},
    description: {type: String, required: true},
})


export default mongoose.model('Orders', Orders);
