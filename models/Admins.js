import mongoose from 'mongoose'

const Admins = new mongoose.Schema({
    username: {type: String, required: true},
    first_name: {type: String, required: true},
    password: {type: String, required: true},
    phone: {type: Number, required: true},
})

export default mongoose.model('Admins', Admins);