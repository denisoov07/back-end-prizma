import Router from 'express'
import PostController from './middlewaree/PostController.js'
import AuthController from "./controllers/AuthController.js";
import {check} from "express-validator";
import authMiddlewaree from './middlewaree/authMiddlewaree.js'
import OrdersController from "./middlewaree/OrdersController.js";
import ListVacanciesController from "./middlewaree/ListVacanciesController.js";
const router = new Router();

//API для работы с комментариями
router.post('/posts/create', PostController.create);
router.post('/posts/approve', authMiddlewaree, PostController.approveAccess);
router.get('/posts', PostController.getAllPosts);
router.delete('/posts/delete/:id', authMiddlewaree, PostController.deleteCurrentPost);

//API для работы с заявками
router.post('/order/create', OrdersController.createOrder);
router.get('/orders', authMiddlewaree, OrdersController.getAllOrders);
router.delete('/order/delete/:id', authMiddlewaree, OrdersController.deleteCurrentOrder);

//API для работы со списком предоставляемых вакансий
router.post('/list/vacancies/create', authMiddlewaree, ListVacanciesController.createVacancy);
router.get('/list/vacancies', ListVacanciesController.getAllListVacancies);
router.delete('/list/vacancy/delete/:id', authMiddlewaree, ListVacanciesController.deleteCurrentVacancy);

//API для работы с адм.
// router.post('/registration', [
//     check('username', 'Логин не может быть пустым').notEmpty(),
//     check('phone', 'Телефон не может быть пустым').notEmpty(),
//     check('first_name', 'Имя не может быть пустым').notEmpty(),
//     check('password', 'Пароль должен быть больше 4 и меньше 10 символов').notEmpty().isLength({min: 4, max: 10}),
// ], AuthController.registartion);
router.post('/login', AuthController.login);

export default router;
