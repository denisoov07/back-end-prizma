import express from 'express'
import mongoose from 'mongoose'
import router from './router.js';
import path from 'path'
import cors from 'cors'
const port = 5000;
// const __dirname = path.resolve();
// const DB_URL = `mongodb://prizmahm:FiKZjVgTY2u7aPNo@cluster0-shard-00-00.eomqz.mongodb.net:27017,cluster0-shard-00-01.eomqz.mongodb.net:27017,cluster0-shard-00-02.eomqz.mongodb.net:27017/myFirstDatabase?ssl=true&replicaSet=atlas-8sntlf-shard-0&authSource=admin&retryWrites=true&w=majority`

const DB_URL = `mongodb://comps:DKLYcZCUu06WFfwO@cluster0-shard-00-00.wincr.mongodb.net:27017,cluster0-shard-00-01.wincr.mongodb.net:27017,cluster0-shard-00-02.wincr.mongodb.net:27017/myFirstDatabase?ssl=true&replicaSet=atlas-k77ze4-shard-0&authSource=admin&retryWrites=true&w=majority`


const app = express();

app.use(express.json());
app.use('/api', router);
app.use(cors());
// app.use(express.static(__dirname + '/views'));
// app.get('*', (req, res) => {
//     res.sendFile(path.resolve(__dirname, 'views', 'index.html'));
// })


const start = async () => {
    try {
        await mongoose.connect(DB_URL, {useUnifiedTopology: true, useNewUrlParser: true});
        app.listen(port, () => {
            console.log(`Starting server: localhost:${port}`)
        });
    } catch(err) {
        console.log("Сервер не запустился", err);
    }
}
start();