import Orders from '../models/Orders.js';

class OrdersController {
    async createOrder(req, res) {
        try {

            const {first_name, email, phone, description} = req.body;
            const vacancy = await Orders.create({first_name, email, phone, description});
            return res.json(vacancy);

        } catch(err) {
            return res.status(500).json(err);
        }
    }

    async getAllOrders(req, res) {
        try {
            const vacancies = await Orders.find();
            return res.json(vacancies);
        } catch(err) {
            return res.status(500).json(err);
        }
    }

    async deleteCurrentOrder(req, res) {
        try {
            const {id} = req.params;
            if (!id) {
                return res.status(400).json({message: 'Не указан ID'});
            }
            const vacancy = await Orders.findByIdAndDelete(id);
            return res.json(vacancy);
        } catch(err) {
            return res.status(500).json(err);
        }
    }
}

export default new OrdersController();