import Post from '../models/Posts.js';

class PostController {
    async create(req, res) {
        try {   
    
            const {first_name, title, content, rate, city, approve} = req.body;
            const post = await Post.create({first_name, title, content, rate, city, approve});
            return res.json(post);
    
        } catch(err) {
            return res.status(500).json(err);
        }
    }

    async getAllPosts(req, res) {
        try {

            const posts = await Post.find();
            return res.json(posts);

        } catch(err) {
            return res.status(500).json(err);
        }
    }
    async approveAccess(req, res) {
        try {

            const post = req.body;
            if (!post.id) {
                return res.status(400).json({message: 'Не указан ID'});
            }
            const updatePost = await Post.findByIdAndUpdate(post.id, post, {new: true})
            console.log(updatePost);
            return res.json(updatePost);

        } catch(err) {
            return res.status(500).json(err);
        }
    }
    async deleteCurrentPost(req, res) {
        try {

            const {id} = req.params;    
            if (!id) {
                return res.status(400).json({message: 'Не указан ID'});
            }
            const post = await Post.findByIdAndDelete(id);
            return res.json(post);

        } catch(err) {
            return res.status(500).json(err);
        }
    }
}

export default new PostController();