import JWT from 'jsonwebtoken';
import secret from '../config.js';
export default function (req, res, next) {
    if (req.method === 'OPTIONS') {
        next()
    }
    else {
        try {
            const token = req.headers.authorization.split(' ')[1];
            if (!token) {
                res.status(400).json({message: "Пользователь не авторизован"})
            }
            else {
                const  decodeData = JWT.verify(token, secret);
                req.user = decodeData;
                next();
            }
        } catch (err) {
            console.log(err);
            res.status(400).json({message: "Пользователь не авторизован"})

        }
    }
}