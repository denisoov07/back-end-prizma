import ListVacancies from "../models/ListVacancies.js";

class ListVacanciesController {
    async createVacancy(req, res) {
        try {

            const {type_job, description} = req.body;
            const vacancy = await ListVacancies.create({type_job, description});
            return res.json(vacancy);

        } catch(err) {
            return res.status(500).json(err);
        }
    }

    async getAllListVacancies(req, res) {
        try {
            const vacancies = await ListVacancies.find();
            return res.json(vacancies);
        } catch(err) {
            return res.status(500).json(err);
        }
    }

    async deleteCurrentVacancy(req, res) {
        try {
            const {id} = req.params;
            if (!id) {
                return res.status(400).json({message: 'Не указан ID'});
            }
            const vacancy = await ListVacancies.findByIdAndDelete(id);
            return res.json(vacancy);
        } catch(err) {
            return res.status(500).json(err);
        }
    }
}

export default new ListVacanciesController();